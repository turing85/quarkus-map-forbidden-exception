# quarkus-map-forbidden-exception

This project shows how to customize the response for built-in exceptions thrown 
by the quarkus runtime. In this example we are using the `ForbiddenException` 
form `quarkus-security`, hence the `quarkus-security` dependency.

## Setup

We create a single endpoint that always throws a `ForbiddenException` in 
[`MyResource.java`](src/main/java/de/turing85/MyResource.java):

    @Path("/hello")
    public class MyResource {
    
        @GET
        @Produces(MediaType.TEXT_PLAIN)
        public String hello() {
            throw new ForbiddenException();
        }
    }

If we build (`./mvnw quarkus:dev`) and test 
(`curl -v http://localhost:8080/hello`) our implementation, we see the default
exception mapper of the quarkus environment taking effect:

    ~$ curl -v http://localhost:8080/hello
    *   Trying 127.0.0.1...
    * Connected to localhost (127.0.0.1) port 8080 (#0)
    > GET /hello HTTP/1.1
    > Host: localhost:8080
    > User-Agent: curl/7.47.0
    > Accept: */*
    >
    < HTTP/1.1 403 Forbidden
    < Content-Length: 0
    <
    * Connection #0 to host localhost left intact

Now to customize this response, we create an [`ExceptionMapper`](https://jakarta.ee/specifications/restful-ws/2.1/apidocs/javax/ws/rs/ext/exceptionmapper)
tot handle `ForbiddenException`s in [`MyForbiddenExceptionMapper`](src/main/java/de/turing85/MyForbiddenExceptionMapper.java):

    @Provider
    public class MyForbiddenExceptionMapper implements ExceptionMapper<ForbiddenException> {
    
      @Override
      public Response toResponse(ForbiddenException exception) {
        return Response.ok("HAIYAA!").build();
      }
    }

If we kept quarkus running in dev-mode and re-execute the `curl`-command, we see

    curl -v http://localhost:8080/hello
    *   Trying 127.0.0.1...
    * Connected to localhost (127.0.0.1) port 8080 (#0)
    > GET /hello HTTP/1.1
    > Host: localhost:8080
    > User-Agent: curl/7.47.0
    > Accept: */*
    >
    < HTTP/1.1 200 OK
    < Content-Length: 7
    < Content-Type: text/plain;charset=UTF-8
    <
    * Connection #0 to host localhost left intact
    HAIYAA!

... and that's all :)